/*
	作者:一只程序缘
	程序整理日期:2019/12/09
	好多人催我啊 今天终于整理这个 PID温控 的程序了
	视频链接	https://www.bilibili.com/video/av77429495
	别忘了点个关注鸭
*/
/*
	本程序用到的资源
	TIM3	用于按键扫描与时间标志位的更新
	TIM14	用于输出PWM控制电热丝加热时间
	
	PG9		DS18B20
	PF9		连接继电器/同时也是DS0
*/
/*
备注:
	小伙伴们可以加上按键的操作
	扫描函数已经写好啦
	只需要在此界面最下面void KeyAction(unsigned char key)函数添加你的操作就好啦
	比如你可以通过按键随时调节参数等
*/

#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "lcd.h"
#include "ds18b20.h"
#include "pid.h"
#include "timer.h"
#include "keyboard.h"
#include "pwm.h"

unsigned int TEMP;						//储存到小数一位的实际实时温度
unsigned int SETTEMP;					//储存到小数一位的实际设置温度
unsigned int PWM=2500;
u8 flag100ms=0;

void KeyAction(unsigned char key);
void ShowTemp(void);
void ShowSetTemp(void);
void ShowPWM(void);
  
int main(void)
{
	unsigned char flag500ms=0;
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);		//设置系统中断优先级分组2
	delay_init(168);  									//初始化延时函数
	uart_init(115200);									//初始化串口波特率为115200
	LED_Init();											//初始化LED
 	LCD_Init();
	TIM3_Int_Init(10-1,8400-1);							//分频系数8400 计数频率10KHz 计数到10溢出 每1Ms产生中断
	TIM14_PWM_Init(10000-1,8400-1);
	KEY_Init();
	PID_Init();
	
	POINT_COLOR=RED;//设置字体为红色 
	LCD_ShowString(30,50,200,16,16,"Explorer STM32F4");	
	LCD_ShowString(30,70,200,16,16,"DS18B20 PID TEST");	
	LCD_ShowString(30,90,200,16,16,"ATOM@ALIENTEK");
	LCD_ShowString(30,110,200,16,16,"2019/11/27");
	
 	while(DS18B20_Init())									//DS18B20初始化	
	{
		LCD_ShowString(30,130,200,16,16,"DS18B20 Error");
		delay_ms(200);
		LCD_Fill(30,130,239,130+16,WHITE);
 		delay_ms(200);
	}
	LCD_ShowString(30,130,200,16,16,"DS18B20 OK");
	
	POINT_COLOR=BLUE;										//设置字体为蓝色 
 	LCD_ShowString(30,150,200,24,24,"TEMP:   . C");
	LCD_ShowString(30,180,200,24,24,"SET :   . C");
	LCD_ShowString(30,210,200,24,24,"PWM :     /9999");
	SETTEMP=999;											//设定初始温度为500
	LCD_DrawLine(0,800-(SETTEMP/2),480,800-(SETTEMP/2));
	ShowSetTemp();
	ShowPWM();
	
	TIM_SetCompare1(TIM14,PWM);								//最大为10000 控制占空比
	while(1)
	{
		KeyDriver();										//按键监控函数
 		if(flag100ms)										//每100ms更新一次实时温度
		{
			flag100ms=0;
			LED1=~LED1;
			
			flag500ms++;
			if(flag500ms==5)
			{
				flag500ms=0;
				ShowTemp();									//500ms测量一次温度
				PWM=PID_realize();
				TIM_SetCompare1(TIM14,PWM);
				ShowPWM();
			}
		}
	 	delay_ms(10);
	}
}

/****************************************************************************************/
void ShowSetTemp(void)
{
	LCD_ShowNum(30+60+12,180,SETTEMP/10,2,24);	//显示正数部分	    
	LCD_ShowNum(30+60+48,180,SETTEMP%10,1,24);	//显示小数部分 
}

void ShowPWM(void)
{
	LCD_ShowNum(30+60+12,210,PWM,4,24);
}

void ShowTemp(void)
{
	static int x=0;
	short temperature;  
	
	temperature=DS18B20_Get_Temp();
	TEMP=temperature;
	if(temperature<0)
	{
		LCD_ShowChar(30+40,150,'-',16,0);			//显示负号
		temperature=-temperature;					//转为正数
	}
	else
	{				
		LCD_ShowChar(30+40,150,' ',16,0);			//去掉负号
	}
	LCD_ShowNum(30+60+12,150,temperature/10,2,24);	//显示正数部分	    
	LCD_ShowNum(30+60+48,150,temperature%10,1,24);	//显示小数部分
	
	//给当前温度打点
	x++;if(x>=480)x=0;
	LCD_DrawPoint(x,800-(TEMP/2));
}

void KeyAction(unsigned char key)
{
	switch(key)
	{
		case 1:LED0=0;break;
		case 2:LED0=1;break;
		case 3:LED0=~LED0;break;
		case 4:LED0=~LED0;break;
		default:break;
	}
}


















