#ifndef __KEY_H
#define __KEY_H

#include "sys.h"

#define KEY0	GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_4)
#define KEY1	GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_3)
#define KEY2	GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_2)
#define WK_UP	GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0)

#define KEY0_PRES	1
#define KEY1_PRES	2
#define KEY2_PRES	3
#define WKUP_PRES	4

extern unsigned char KeySta[4];							//按键目前状态
extern unsigned long KeyDownTime[4];					//按键按下时间累积
extern unsigned char KeyCodeMap[4];						//按键编号

void KEY_Init(void);
void KeyScan(void);
void KeyDriver(void);
extern void KeyAction(unsigned char key);

#endif

