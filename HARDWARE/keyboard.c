#include "keyboard.h"
#include "delay.h"

unsigned char KeySta[4]={1,1,1,1};							//按键目前状态
unsigned long KeyDownTime[4]={0,0,0,0};						//按键按下时间累积
unsigned char KeyCodeMap[4]={1,2,3,4};						//按键编号

void KEY_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA|RCC_AHB1Periph_GPIOE,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin		=	GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode	=	GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_Speed	=	GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd	=	GPIO_PuPd_UP;
	
	GPIO_Init(GPIOE,&GPIO_InitStructure);			//初始化3个按键 E2 E3 E4
	
	GPIO_InitStructure.GPIO_Pin		=	GPIO_Pin_0;
	GPIO_InitStructure.GPIO_PuPd	=	GPIO_PuPd_DOWN;
	
	GPIO_Init(GPIOA,&GPIO_InitStructure);			//初始化另外一个按键 A0
}

//按键扫描函数 函数1ms运行一次 在某个定时器中断中
void KeyScan(void)
{
	unsigned char i;
	static unsigned char keybuf[4]={			//按键扫描缓冲区
	0xff,0xff,0xff,0xff};
	
	keybuf[0]=(keybuf[0] << 1)|KEY0;			//是0为正常按下按键
	keybuf[1]=(keybuf[1] << 1)|KEY1;
	keybuf[2]=(keybuf[2] << 1)|KEY2;
	keybuf[3]=(keybuf[3] << 1)|(!WK_UP);		//是1为正常按下按键  WK_UP一般情况为0 按下为1
	
	for(i=0;i<4;i++)								//按下8ms才稳定
	{
		if((keybuf[i] & 0xff)==0x00)				//按键稳定按下
		{
			KeySta[i]=0;
			KeyDownTime[i] += 4;					//4ms加4相当于1ms加1
		}
		else if((keybuf[i] & 0xff)==0xff)			//按键稳定弹起
		{
			KeySta[i]=1;
			KeyDownTime[i]=0;
		}
	}
}

//按键驱动函数，在主函数的while循环中
void KeyDriver(void)
{
	unsigned char i;
	static unsigned char backup[4]={1,1,1,1};					//ACR_BYTE0_ADDRESS没有按下时默认是1
	static unsigned long TimeThr[4]={1000,1000,1000,1000};		//1秒触发
	
	for(i=0;i<4;i++)
	{
		if(backup[i] != KeySta[i])
		{
			if(backup[i] == 0)
			{
				KeyAction(KeyCodeMap[i]);						//按下后弹起一瞬间执行动作
			}
			backup[i] = KeySta[i];
		}
		if(KeyDownTime[i]>0)
		{
			if(KeyDownTime[i] >= TimeThr[i])					//长按1秒达到触发
			{
				KeyAction(KeyCodeMap[i]);
				TimeThr[i] += 200;								//确保下一次满足上面if是在200ms以后
			}
		}
		else
		{
			TimeThr[i]=1000;
		}
	}
}

