#include "pwm.h"

void TIM14_PWM_Init(u32 arr,u32 psc)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14,ENABLE);			//使能TIM14时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF,ENABLE);			//使能io口时钟
	
	GPIO_PinAFConfig(GPIOF,GPIO_PinSource9,GPIO_AF_TIM14);			//设置F9为TIM14的复用
	
	GPIO_InitStructure.GPIO_Pin		=	GPIO_Pin_9;					//led0和led1对应的io口
	GPIO_InitStructure.GPIO_Mode	=	GPIO_Mode_AF;				//普通输出模式
	GPIO_InitStructure.GPIO_OType	=	GPIO_OType_PP;				//推挽输出
	GPIO_InitStructure.GPIO_Speed	=	GPIO_Speed_100MHz;			//高速模式
	GPIO_InitStructure.GPIO_PuPd	=	GPIO_PuPd_UP;				//上拉
	GPIO_Init(GPIOF,&GPIO_InitStructure);
	
	TIM_TimeBaseStructure.TIM_Period		=	arr;				//初始化定时器14
	TIM_TimeBaseStructure.TIM_Prescaler		=	psc;
	TIM_TimeBaseStructure.TIM_CounterMode	=	TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_ClockDivision	=	TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM14,&TIM_TimeBaseStructure);
	
	TIM_OCInitStructure.TIM_OCMode		=	TIM_OCMode_PWM1;		//pwm调制模式1
	TIM_OCInitStructure.TIM_OutputState	=	TIM_OutputState_Enable;	//比较输出使能
	TIM_OCInitStructure.TIM_OCPolarity	=	TIM_OCPolarity_High;		//输出极性低	
	TIM_OC1Init(TIM14,&TIM_OCInitStructure);
	
	TIM_OC1PreloadConfig(TIM14,TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM14,ENABLE);
	TIM_Cmd(TIM14,ENABLE);
}





