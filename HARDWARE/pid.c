#include "pid.h"
#include "lcd.h"

extern unsigned int TEMP;						//储存到小数一位的实际实时温度
extern unsigned int SETTEMP;					//储存到小数一位的实际设置温度

struct PID pid;

void PID_Init(void)					//因为有储存，因此用ReadPID()代替
{
	pid.Set=SETTEMP;
	pid.Actual=TEMP;
	pid.err=0;
	pid.err_last=0;                                                                                                                                                                                                   

	pid.Kp=50;						//实际为除1倍
	pid.Ki=100;						//实际为除1倍
	pid.Kd=350;						//实际为除1倍
	pid.Kout=800;						//偏移值
	pid.voltage=0;
	pid.integral=0;
	
	LCD_ShowNum(300,120,pid.Kp,3,24);
	LCD_ShowNum(300,150,pid.Ki,3,24);
	LCD_ShowNum(300,180,pid.Kd,3,24);
	LCD_ShowNum(300,210,pid.Kout,3,24);
}

int PID_realize(void)
{
	int t;
	
	pid.Set      = SETTEMP;					//设定值 700
	pid.Actual   = TEMP;					//实际值 100-700
	pid.err      = pid.Set - pid.Actual;	//之差
	

	pid.integral = pid.integral + pid.err;
	LCD_ShowNum(30+60+12,240,pid.integral,6,24);

	pid.voltage = pid.Kp*pid.err								//设定值与实际的偏差
				+ pid.Ki*pid.integral/1000						//历史累计偏差
				+ pid.Kd*(pid.err-pid.err_last)					//本次偏差与上次偏差比较
				+ pid.Kout;										//最终计算值
	
	pid.err_last = pid.err;
	
	t=pid.voltage;
	
	if(t>9999)		t=9998;
	else if(t<0)	t=2;

	return t;
}













